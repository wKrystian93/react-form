import React, {Component} from 'react';
import './App.css';

const emailValidator = new RegExp(/\S+@\S+\.\S+/);

const passwordValidator = require('password-validator');
const passwordSchema = new passwordValidator();
passwordSchema
    .is().min(5)                                    // Minimum length 8
    .is().max(15)                                  // Maximum length 100
    .has().uppercase()                              // Must have uppercase letters
    .has().lowercase()                              // Must have lowercase letters
    .has().digits()                                 // Must have digits
    .has().not().spaces()                           // Should not have spaces
    .has().symbols();                         // specifies password must include symbols


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            email: "",
            password: "",

            formErrors: {
                name: "",
                email: "",
                password: "",
            }
        }
    }

    handleSubmit = () => {

    };

    handleChange = e => {
        const {name, value} = e.target;
        let formErrors = {...this.state.formErrors};

        switch (name) {
            case "name":
                if (value.length > 3 || value.length === 0) {
                    formErrors.name = '';
                } else {
                    formErrors.name = 'name must contain more than 3 characters';
                }
                break;

            case "email":
                if (emailValidator.test(value) || value.length === 0) {
                    formErrors.email = "";
                } else {
                    formErrors.email = 'wrong email'
                }
                break;

            case "password":
                const errors = passwordSchema.validate(value, {list: true})
                if (passwordSchema.validate(value) || value.length === 0) {
                    formErrors.password = '';
                } else {
                    formErrors.password = `wrong password: ${errors}`;
                }
                break;

            default:
                break;
        }

        this.setState({formErrors, [name]: value});
    }

    render() {
        const {
            name: nameError,
            email: emailError,
            password: passwordError
        } = this.state.formErrors;

        return (
            <div className="container d-flex flex-column justify-content-center col-lg-4 h-100 ">
                <form onSubmit={this.handleSubmit} className="card p-4 bg-light">
                    <div className="form-group">
                        <label htmlFor="Name">Name</label>
                        <input
                            onChange={this.handleChange}
                            type="text"
                            name="name"
                            className="form-control"
                            id="Name"
                            placeholder="Name"/>

                        <small id="emailHelp" className="form-text text-muted">{nameError}</small>
                    </div>

                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Email address</label>
                        <input
                            onChange={this.handleChange}
                            type="email"
                            name="email"
                            className="form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter email"/>

                        <small id="emailHelp" className="form-text text-muted">{emailError}</small>
                    </div>

                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input
                            onChange={this.handleChange}
                            type="password"
                            name="password"
                            className="form-control"
                            id="exampleInputPassword1"
                            placeholder="Password"/>

                        <small id="emailHelp" className="form-text text-muted">{passwordError}</small>
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        );
    }

}

export default App;